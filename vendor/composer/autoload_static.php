<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit6f44168a08f95e7f1b53cab4a0290fa9
{
    public static $prefixLengthsPsr4 = array (
        'a' => 
        array (
            'app\\' => 4,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'app\\' => 
        array (
            0 => __DIR__ . '/../..' . '/app',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit6f44168a08f95e7f1b53cab4a0290fa9::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit6f44168a08f95e7f1b53cab4a0290fa9::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
